
## How to use :

- Clone/Download the directory and navigate to each folder. Or...
- Simply navigate to the folder and download/copy the scripts! It's *that* simple and easy.
- Setup virtual environment for that scripts:
    - `python -m venv env`
    - `source env\bin\activate`
    - `pip install -r requirements.txt`

- Run the script :)

Remember to star the repo if you love the scripts~ :wink:

## Contribution Guidelines :

### Steps required to follow before adding any script 

- Make a **separate folder** for your script.
- There shouldn't be any **spaces** between the names of the script. (Use underscore or dash Symbol)
	- :x: Script One
	- :heavy_check_mark: Script_One
	- :heavy_check_mark: Script-One

- The Folder should contain the followings -
	- Main Python Script,
	- Supporting files for the Script (If any)
	-  A separate `README.md` File with proper documentation.
    - `requirements.txt` file for any required library.

    - How to create `requirements.txt` file
            - `cd <your_script_dir>`
            - Activate your local virtual environment
                - `source env\bin\activate`
            - `pip freeze > requirements.txt`

